-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2020 at 03:45 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `disaster_notification`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eventboard`
--

CREATE TABLE `tbl_eventboard` (
  `event_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `content` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `audience` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eventboard`
--

INSERT INTO `tbl_eventboard` (`event_id`, `title`, `content`, `user_id`, `date_added`, `audience`) VALUES
(1, 'test', 'this is a test event.', 0, '2020-05-05 00:00:00', 0),
(2, 'test teacher', 'this is a test event.', 0, '2020-05-05 00:00:00', 1),
(6, 'test student', 'This is a test.', 0, '2020-05-05 00:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_middle_name` varchar(25) NOT NULL,
  `user_last_name` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `user_status` text NOT NULL,
  `user_token` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_middle_name`, `user_last_name`, `username`, `password`, `user_status`, `user_token`, `date_added`) VALUES
(1, 'Subaru', 'S', 'Sybra', '1', '1', '1', '2121dasdaccedadca32qcawcawe4casc4as4cac4aw4ca', '0000-00-00 00:00:00'),
(2, 'Kuto', 'D', 'Sybra', '2', '2', '2', '2121d2131accedad3ca32qcawcawe4casc4as4cac4aw4casas231', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_eventboard`
--
ALTER TABLE `tbl_eventboard`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_eventboard`
--
ALTER TABLE `tbl_eventboard`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
