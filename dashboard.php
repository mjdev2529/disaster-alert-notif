<?php

  session_start();

  if($_SESSION["in"] != 1){
    echo "<script>alert('Please sign in first!'); window.location='index.php';</script>";
  }

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CHMSC Disaster Alert Notification</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/dataTables.bootstrap4.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css"> -->
 
    <!-- JS -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" src="../assets/plugins/datatables/dataTables.bootstrap4.js"></script> -->
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!--  <script type="text/javascript" src="assets/plugins/select2/select2.full.min.js"></script> -->
    <script src="assets/plugins/feather/feather.min.js"></script>
    <!-- <script src="assets/plugins/chart.js/Chart.min.js"></script> -->

    <link rel="icon" type="image/gif/png" href="../favicon.png" />
    <style type="text/css">
      body {
        font-size: .875rem;
        background-color: #012b54; /* For browsers that do not support gradients */
        background-image: linear-gradient(to right, #012b54 , #ff7600); /* Standard syntax (must be last) */
      }

      .feather {
        width: 16px;
        height: 16px;
        vertical-align: text-bottom;
      }

      /*
       * Sidebar
       */

      .sidebar {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        z-index: 100; /* Behind the navbar */
        padding: 48px 0 0; /* Height of navbar */
        box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);
      }

      .sidebar-sticky {
        position: relative;
        top: 0;
        height: calc(100vh - 48px);
        padding-top: .5rem;
        overflow-x: hidden;
        overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
      }

      @supports ((position: -webkit-sticky) or (position: sticky)) {
        .sidebar-sticky {
          position: -webkit-sticky;
          position: sticky;
        }
      }

      .sidebar .nav-link {
        font-weight: 500;
        color: #333;
      }

      .sidebar .nav-link .feather {
        margin-right: 4px;
        color: #999;
      }

      .sidebar .nav-link.active {
        color: #007bff;
      }

      .sidebar .nav-link:hover .feather,
      .sidebar .nav-link.active .feather {
        color: inherit;
      }

      .sidebar-heading {
        font-size: .75rem;
        text-transform: uppercase;
      }

      /*
       * Content
       */

      [role="main"] {
        padding-top: 133px; /* Space for fixed navbar */
      }

      @media (min-width: 768px) {
        [role="main"] {
          padding-top: 48px; /* Space for fixed navbar */
        }
      }

      /*
       * Navbar
       */

      .navbar-brand {
        padding-top: .75rem;
        padding-bottom: .75rem;
        font-size: 1rem;
        background-color: rgba(0, 0, 0, .25);
        box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);
      }

      .navbar .form-control {
        padding: .75rem 1rem;
        border-width: 0;
        border-radius: 0;
      }

      .form-control-dark {
        color: #fff;
        background-color: rgba(255, 255, 255, .1);
        border-color: rgba(255, 255, 255, .1);
      }

      .form-control-dark:focus {
        border-color: transparent;
        box-shadow: 0 0 0 3px rgba(255, 255, 255, .25);
      }

      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .bg-blue{
        background-color: #012b54!important;
      }

      .btn-blue{
        background-color: #012b54!important;
        color: white!important;
      }
      .text-blue{
        color: #012b54!important;
      }

      .bg-orange{
        background-color: #a24006!important;
      }

      /*#grad1 {
        background-color: #012b54;
        background-image: linear-gradient(to right, #012b54 , #ff7600);
        height: 100vh;
        overflow: auto;
      }*/

      #map {
        height: 50vh;
      }

      #map1 {
        height: 50vh;
      }

</style>
  </head>
  <body class="bg-light">

    <nav class="navbar navbar-dark fixed-top bg-blue flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Disaster Alert & Notification</a>
      <!-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"> -->
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#" onclick="logout()">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="#" onclick="page_data('tbl_alert_notif')">
                  <span data-feather="alert-triangle"></span>
                  Disaster <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" onclick="page_data('tbl_event_board')">
                  <span data-feather="clipboard"></span>
                  Event Board
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" onclick="page_data('tbl_safety_tips')">
                  <span data-feather="info"></span>
                  Safety Tips
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" onclick="page_data('tbl_users')">
                  <span data-feather="users"></span>
                  Users
                </a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="shopping-cart"></span>
                  Products
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="users"></span>
                  Customers
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="bar-chart-2"></span>
                  Reports
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="layers"></span>
                  Integrations
                </a>
              </li>
            </ul>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Saved reports</span>
              <a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
                <span data-feather="plus-circle"></span>
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Current month
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Last quarter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Social engagement
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Year-end sale
                </a>
              </li>
            </ul> -->
          </div>
        </nav>

        <main id="grad1" role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 pb-3">
          
          <h1 class="h2 mt-3 text-white"></h1>
          <div class="card mb-3 height-400 tbl_alert_notif">
            <div class="card-body">

              <!-- <h5 class="card-title">Card title</h5> -->
              <h6 class="card-subtitle mb-2"><i data-feather="alert-triangle"></i> Disaster Alert</h6>
              <div class="table-responsive col-md-12">
                <div class="row">
                  <div class="col-md-4">
                  <button type="button" class="btn btn-danger btn-lg btn-block" onclick="new_alert(1)">FIRE</button>
                  </div>
                  <div class="col-md-4">
                    <button type="button" class="btn btn-warning btn-lg btn-block" onclick="new_alert(2)">EARTHQUAKE</button>
                  </div>
                  <div class="col-md-4">
                    <button type="button" class="btn btn-info btn-lg btn-block" onclick="new_alert(3)">FLOOD/STORM</button>
                  </div>
                </div>
              </div>

            </div>

            <div class="card-body alert_template">
              <!-- <h5 class="card-title">Card title</h5> -->
              <h6 class="card-subtitle mb-2"><i data-feather="file-text"></i> Alert Template</h6>
              <!-- <div class="btn-group mb-2 float-right">
                <button class="btn btn-blue btn-sm" data-toggle="modal" data-target="#alert_add_md"><i class="fa fa-plus-circle"></i></button>
                <button class="btn btn-danger btn-sm" onclick="delete_alert()"><i class="fa fa-trash"></i></button>
              </div> -->
              <div id="tbl_alert_template" class="table-responsive">
                <table width="100%" class="table table-striped table-sm" id="tbl_notif_temp">
                  <thead class="bg-dark text-white">
                    <tr>
                      <!-- <th width="5px"><input type="checkbox" id="cb_alert_temp" onclick="checkAllAlert()"></th> -->
                        <th width="10px">#</th>
                        <th width="10px"></th>
                        <th>Disaster Type</th>
                        <!-- <th width="100px">Danger Level</th>
                        <th width="115px">Set as Template</th> -->
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="card-body alert_data">
              <!-- <h5 class="card-title">Card title</h5> -->
              <h6 class="card-subtitle mb-2"><i data-feather="file-text"></i> Past Alerts</h6>
              <!-- <div class="btn-group mb-2 float-right">
                <button class="btn btn-blue btn-sm" data-toggle="modal" data-target="#alert_add_md"><i class="fa fa-plus-circle"></i></button>
                <button class="btn btn-danger btn-sm" onclick="delete_alert()"><i class="fa fa-trash"></i></button>
              </div> -->
              <div id="tbl_alert_notif" class="table-responsive">
                <table width="100%" class="table table-striped table-sm" id="tbl_notif">
                  <thead class="bg-dark text-white">
                    <tr>
                      <!-- <th width="5px"><input type="checkbox" id="cb_alerts" onclick="checkAllAlert()"></th> -->
                        <th width="10px">#</th>
                        <!-- <th width="10px"></th> -->
                        <th width="100px">Date</th>
                        <th>Title</th>
                        <!-- <th width="100px">Danger Level</th> -->
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>

          </div>

          <div class="card mb-3 height-400 tbl_event_board">
            <div class="card-body">
              <!-- <h5 class="card-title">Card title</h5> -->
              <h6 class="card-subtitle mb-2"><i data-feather="clipboard"></i> Event Board</h6>
              <div class="btn-group mb-2 float-right">
                <button class="btn btn-blue btn-sm" data-toggle="modal" data-target="#add_event_md"><i class="fa fa-plus-circle"></i></button>
                <button class="btn btn-danger btn-sm" onclick="delete_event()"><i class="fa fa-trash"></i></button>
              </div>
              <div id="tbl_event_board" class="table-responsive">
                <table width="100%" class="table table-striped table-sm" id="tbl_events">
                  <thead class="bg-dark text-white">
                    <tr>
                      <th width="5px"><input type="checkbox" id="cb_events" onclick="checkAllData()"></th>
                        <th width="10px">#</th>
                        <th width="10px"></th>
                        <th width="100px">Date</th>
                        <th>Title</th>
                        <th width="100px">Audience</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="card mb-3 height-400 tbl_safety_tips">
            <div class="card-body">
              <!-- <h5 class="card-title">Card title</h5> -->
              <h6 class="card-subtitle mb-2"><i data-feather="info"></i> Safety Tips</h6>
              <div class="btn-group mb-2 float-right">
                <button class="btn btn-blue btn-sm" data-toggle="modal" data-target="#add_tips_md"><i class="fa fa-plus-circle"></i></button>
                <button class="btn btn-danger btn-sm" onclick="delete_tips()"><i class="fa fa-trash"></i></button>
              </div>
              <div id="tbl_safety_tips" class="table-responsive">
                <table width="100%" class="table table-striped table-sm" id="tbl_tips">
                  <thead class="bg-dark text-white">
                    <tr>
                      <th width="5px"><input type="checkbox" id="cb_events" onclick="checkAllData()"></th>
                        <th width="10px">#</th>
                        <th width="10px"></th>
                        <th width="100px">Date</th>
                        <th>Title</th>
                        <th width="100px">Audience</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <!-- <h2>Section title</h2> -->
          <div class="card tbl_users">
              <div class="card-body">
                <!-- <h5 class="card-title">Card title</h5> -->
                <h6 class="card-subtitle mb-2"><i data-feather="users"></i> Users</h6>
                <div class="btn-group mb-2 float-right">
                  <button class="btn btn-blue btn-sm" data-toggle="modal" data-target="#add_user_md" ><i class="fa fa-plus-circle"></i></button>
                  <button class="btn btn-danger btn-sm" onclick="delete_user();"><i class="fa fa-trash"></i></button>
                </div>
                <div id="tbl_sec_title" class="table-responsive">
                  <table width="100%" class="table table-striped table-sm" id="tbl_users">
                    <thead class="bg-dark text-white">
                      <tr>
                        <th width="5px"><input type="checkbox" id="cb_users" onclick="checkAllUser()"></th>
                        <th width="10px">#</th>
                         <th width="10px"></th>
                        <th>Name</th>
                        <th width="100px">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </main>
      </div>
    </div>

<?php 
    include "modals/add_user.php";
    include "modals/edit_user.php";
    include "modals/add_event.php";
    include "modals/edit_events.php";
    include "modals/alert_add.php";
    include "modals/alert_edit.php";
    include "modals/add_tips.php";
    include "modals/edit_tips.php";
  ?>

  </body>
</html>
<script type="text/javascript">

(function () {
  'use strict'

  feather.replace()

 
}())

  $(document).ready( function(){
    getUsers();
    getEvents();
    getNotif();
    getTips();
    getNotif_temp();
  });

  function getUsers(){
    var tbl = $("#tbl_users").DataTable();
    tbl.destroy();
    $("#tbl_users").dataTable({
      "processing": true,
      "ajax": {
          "url": "ajax/datatables/users.php",
          "type": "POST"
      },
      "columns": [
          { 
            "mRender": function(data,type,row){
              return "<input type='checkbox' name='u_id' value='"+row.u_id+"'>";
            } 
          },
          { "data": "count" },
          { 
            "mRender": function(data,type,row){
              return "<button class='btn btn-sm btn-outline-dark' onclick='edit_user("+row.u_id+")'><i class='fa fa-edit'></i></button>";
            } 
          },
          { "data": "uname" },
          { "data": "ustat" }
      ]
    });
  }

  function getEvents(){
    var tbl = $("#tbl_events").DataTable();
    tbl.destroy();
    $("#tbl_events").dataTable({
      "processing": true,
      "ajax": {
          "url": "ajax/datatables/eventboard.php",
          "type": "POST"
      },
      "columns": [
          { 
            "mRender": function(data,type,row){
              return "<input type='checkbox' name='e_id' value='"+row.event_id+"'>";
            } 
          },
          { "data": "count" },
          { 
            "mRender": function(data,type,row){
              return "<button class='btn btn-sm btn-outline-dark' onclick='edit_event("+row.event_id+")'><i class='fa fa-list'></i></button>";
            } 
          },
          { "data": "date_added" },
          { "data": "title" },
          { "data": "audience" }
      ]
    });
  }

  function getTips(){
    var tbl = $("#tbl_tips").DataTable();
    tbl.destroy();
    $("#tbl_tips").dataTable({
      "processing": true,
      "ajax": {
          "url": "ajax/datatables/safety_tips.php",
          "type": "POST"
      },
      "columns": [
          { 
            "mRender": function(data,type,row){
              return "<input type='checkbox' name='t_id' value='"+row.safety_tips_id+"'>";
            } 
          },
          { "data": "count" },
          { 
            "mRender": function(data,type,row){
              return "<button class='btn btn-sm btn-outline-dark' onclick='edit_tips("+row.safety_tips_id+")'><i class='fa fa-list'></i></button>";
            } 
          },
          { "data": "date_added" },
          { "data": "title" },
          { "data": "audience" }
      ]
    });
  }

  function getNotif(){
   var tbl = $("#tbl_notif").DataTable();
    tbl.destroy();
    $("#tbl_notif").dataTable({
      "processing": true,
      "ajax": {
          "url": "ajax/datatables/alerts.php",
          "type": "POST"
      },
      "columns": [
          // { 
          //   "mRender": function(data,type,row){
          //     return "<input type='checkbox' name='a_id' value='"+row.a_id+"'>";
          //   } 
          // },
          { "data": "count" },
          // { 
          //   "mRender": function(data,type,row){
          //     return "<button class='btn btn-sm btn-outline-dark' onclick='edit_alert("+row.a_id+")'><i class='fa fa-edit'></i></button>";
          //   } 
          // },
          { "data": "date_added" },
          { "data": "title" },
          // { "data": "level" }
      ]
    });
  }

  function getNotif_temp(){
   var tbl = $("#tbl_notif_temp").DataTable();
    tbl.destroy();
    $("#tbl_notif_temp").dataTable({
      "processing": true,
      "ajax": {
          "url": "ajax/datatables/alert_template.php",
          "type": "POST"
      },
      "columns": [
          // { 
          //   "mRender": function(data,type,row){
          //     return "<input type='checkbox' name='at_id' value='"+row.a_id+"'>";
          //   } 
          // },
          { "data": "count" },
          { 
            "mRender": function(data,type,row){
              return "<button class='btn btn-sm btn-outline-dark' onclick='edit_alert("+row.a_id+")'><i class='fa fa-edit'></i></button>";
            } 
          },
          { "data": "title" },
          // { "data": "level" },
          // { "mRender": function(data,type,row){ 
          //     if(row.set_temp == 0){
          //       return "<button class='btn btn-sm btn-outline-success btn-block' onclick='set_temp("+row.a_id+","+row.d_type+",1)'><i class='fa fa-check'></i> Set</button>";
          //     }else{
          //       return "<button class='btn btn-sm btn-outline-danger btn-block' onclick='set_temp("+row.a_id+","+row.d_type+",0)'><i class='fa fa-times'></i> Cancel</button>";
          //     }
          //   } 
          // }
      ]
    });
  }

  function logout(){
    var x = confirm("Are you sure to end your session?");

    if(x){
      window.location="ajax/logout.php";
    }
  }
</script>>
<?php include 'ajax/scripts.php';?>
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZ4IUL91KzgJ8TC6unJSAiywaizMvadeM&callback=initMap"></script>