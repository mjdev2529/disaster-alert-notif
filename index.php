
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CHMSC Disaster Alert & Notification</title>

    <!--- CSS --->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/dataTables.bootstrap4.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/datatables/jquery.dataTables.min.css"> -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css"> -->
 
    <!-- JS -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/plugins/datatables/dataTables.bootstrap4.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!--  <script type="text/javascript" src="assets/plugins/select2/select2.full.min.js"></script> -->

    <link rel="icon" type="image/gif/png" href="../favicon.png" />
  </head>
<style type="text/css">
html,
body {
  height: 100%;
}

body {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}

.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}
.form-signin .checkbox {
  font-weight: 400;
}
.form-signin .form-control {
  position: relative;
  box-sizing: border-box;
  height: auto;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>
  <body>

    <!-- Begin page content -->
    <div class="col-md-12">
      <div class="row">
        <div class="text-center col-md-6 offset-md-3">
          <!-- <div class="alert alert-secondary alert-dismissible fade show" role="alert" style="display: none;">
            <div id="notif">
              
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> -->
        </div>

        <div class="col-md-12">
          <form method="POST" action="" id="form_login" class="form-signin text-center">
            <h1 class="mb-3 font-weight-normal">CHMSC<br> <small>Disaster Alert Notification</small></h1>
            <h5 class="mb-3 font-weight-normal">Please sign in</h5>
            <label for="inputUsername" class="sr-only">Username</label>
            <input type="text" id="inputUsername" class="form-control" placeholder="Username" name="username" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
            <button class="btn btn-lg btn-dark btn-block" type="submit">Sign in</button>
            <p class="mt-5 mb-3 text-muted">&copy; <?php echo date("Y");?></p>
          </form>
        </div>

      </div>
    </div>
  </body>
</html>
<script type="text/javascript">
  $(document).ready( function(){
    $("#form_login").on("submit",function(e){
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
          "type"  :"POST",
          "url"   :"ajax/auth.php",
          "data"  : data,
          "success": function(data)
          {
            $(".btn-dark").prop("disabled", true);
            $(".btn-dark").html("<i class='fa fa-refresh fa-spin'></i>");
            setTimeout( function(){
              if(data == 1){
                $(".btn-dark").prop("disabled", false);
                $(".btn-dark").html("Sign in");
                window.location="dashboard.php";
              }else{
                // $(".alert-info").removeClass("alert-info");
                $(".btn-dark").html("Sign in");
                $(".btn-dark").prop("disabled", false);
                alert("Username or Password Incorrect!");
              }
            },2000);
          }
        });
    });
  });
</script>