<script type="text/javascript">

	$(document).ready( function(){
		page_data('tbl_alert_notif');
		initMap("map");
		$('#alert_add_md').on('shown.bs.modal', function () {
		 	initMap("map");
		});
	});

	//MAPS
		
		let map;
		let markers = [];

		function initMap(map_div){
		  const chmsc_loc = { lat: 10.742869, lng: 122.969473 };
		  map = new google.maps.Map(document.getElementById(map_div), {
		    zoom: 17,
		    center: chmsc_loc,
		  });
		  map.addListener("click", (event) => {
		  	deleteMarkers();
		    addMarker(event.latLng);
		  });
		}

		function addMarker(location) {
		  const marker = new google.maps.Marker({
		    position: location,
		    map: map,
		    label: "Evacuation Center"
		  });
		  markers.push(marker);
		  $("#evac_loc").val(location.toString());
		  $("#evac_loc1").val(location.toString());
		}

		function setMapOnAll(map) {
		  for (let i = 0; i < markers.length; i++) {
		    markers[i].setMap(map);
		  }
		}

		function clearMarkers() {
		  setMapOnAll(null);
		  $("#evac_loc").val("");
		  $("#evac_loc1").val("");
		}

		function showMarkers() {
		  setMapOnAll(map);
		}

		function deleteMarkers() {
		  clearMarkers();
		  markers = [];
		}

	//MAPS END
	function edit_temp(){
		$("#alert_template").show();
	}
	//

	//PAGINATION
	function page_data(page){
		if(page){
			var view_page = "."+page;
			$(".card").hide();
			$(view_page).show();
		}
	}


	//USER
	$("#addUser_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var url = "ajax/add_user.php";
		$("#add_user_md").modal("hide");
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			success: function(data){
				if(data == 1){
					alert("User was successfully added.");
					$("input").val("");
					$("select").val("0");
					getUsers();
				}
			}
		});
	});

	function delete_user(){
	var con = confirm("Are you sure to delete selected?");

	if(con)
	{
		var check = $("input[name=u_id]").is(":checked");
	    var x = [];
	    $("input[name=u_id]:checked").each( function(){
	      x.push($(this).val());
	    });

	    if(check){
	    	$.ajax({
	    		type: "POST",
	    		url: "ajax/delete_user.php",
	    		data: {uID: x},
	    		success: function(data){
	    			if(data == 1){
	    				alert("Selected user was deleted.");
	    				getUsers();
	    			}else{
	    				alert("Error!");
	    			}
	    		}
	    	});
	    }else{
	    	alert("Please select user first!");
	    }
	}
  }

  function checkAllUser(){
    var cb = $("#cb_users").is(":checked");

    if(cb){
      $("input[name=u_id]").prop("checked", true);
    }else{
      $("input[name=u_id]").prop("checked", false);
    }
  }

  function checkAllData(){
    var cb = $("#cb_events").is(":checked");

    if(cb){
      $("input[name=e_id]").prop("checked", true);
    }else{
      $("input[name=e_id]").prop("checked", false);
    }
  }

  function edit_user(id){
  	$("#edit_user_md").modal();
  	getUserData(id);
  	$("#u_id").val(id);
  }

  function getUserData(id){
  	$.ajax({
  		type: "POST",
  		url: "ajax/user_data.php",
  		data: {uID: id},
  		success: function(data){
  			var o = JSON.parse(data);
  			$("#e_ufname").val(o.user_name);
  			$("#e_umname").val(o.user_middle_name);
  			$("#e_ulname").val(o.user_last_name);
  			$("#e_uname").val(o.username);
  			$("#e_upass").val(o.password);
  			$("#e_role").val(o.user_status);
  		}
  	});
  }

  $("#editUser_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var url = "ajax/edit_user.php";
		$("#edit_user_md").modal("hide");
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			success: function(data){
				if(data == 1){
					alert("User data was successfully updated.");
					$("input").val("");
					$("select").val("0");
					getUsers();
				}
			}
		});
	});

  //USER END

  //EVENT BOARD

  	$("#addEvent_form").submit( function(e){
		e.preventDefault();
		var formData = new FormData(this);
		var url = "ajax/add_event.php";
		$("#add_event_md").modal("hide");
		var aud = $("#audience").val();

		if(aud != -1){
			$.ajax({
				type: "POST",
				url: url,
				processData: false,
				contentType: false,
				data: formData,
				success: function(data){
					if(data == 1){
						alert("Event was successfully added.");
						$("input").val("");
						$("textarea").html("");
						$("select").val("-1");
						getEvents();
					}
				}
			});
		}else{
			alert("Please select an audience.");
		}
	});

	function delete_event(){
	var con = confirm("Are you sure to delete selected?");

	if(con)
	{
		var check = $("input[name=e_id]").is(":checked");
	    var x = [];
	    $("input[name=e_id]:checked").each( function(){
	      x.push($(this).val());
	    });

	    if(check){
	    	$.ajax({
	    		type: "POST",
	    		url: "ajax/delete_event.php",
	    		data: {uID: x},
	    		success: function(data){
	    			if(data == 1){
	    				alert("Selected event was deleted.");
	    				getEvents();
	    			}else{
	    				alert("Error!");
	    			}
	    		}
	    	});
	    }else{
	    	alert("Please select event first!");
	    }
	}
  }

   function edit_event(id){
  	$("#edit_event_md").modal();
  	getEventData(id);
  	$("#e_id").val(id);
  }

  function getEventData(id){
  	$.ajax({
  		type: "POST",
  		url: "ajax/event_data.php",
  		data: {uID: id},
  		success: function(data){
  			var o = JSON.parse(data);
  			$("#e_id").val(o.event_id);
  			$("#e_title").val(o.title);
  			$("#e_content").html(o.content);
  			$("#e_audience").val(o.audience);
  			$("#e_prev_header").attr("src",o.img_path);
  		}
  	});
  }

  $("#editEvent_form").submit( function(e){
		e.preventDefault();
		var formData = new FormData(this);
		var url = "ajax/edit_event.php";
		$("#edit_event_md").modal("hide");
		$.ajax({
			type: "POST",
			url: url,
			processData: false,
			contentType: false,
			data: formData,
			success: function(data){
				if(data == 1){
					alert("Event data was successfully updated.");
					$("input").val("");
					$("select").val("0");
					getEvents();
				}
			}
		});
	});

  function getImage_event(){
  	var event = document.getElementById('eheader');
  	var output = document.getElementById('prev_header');
	document.getElementById('prev_header').src = window.URL.createObjectURL(event.files[0]);
  }

  function getImage_edit_event(){
  	var event = document.getElementById('e_header');
  	var output = document.getElementById('e_prev_header');
	document.getElementById('e_prev_header').src = window.URL.createObjectURL(event.files[0]);
  }

  //EVENT BOARD END

 //  //DISASTER ALERT

 	function new_alert(dType){
 		var url = "ajax/alert_data_add.php";
 		if(dType != 0){
			$.ajax({
				type: "POST",
				url: url,
				data: {dType: dType},
				success: function(data){
					if(data == 1){
						alert("Alert was successfully sent!.");
						getNotif();
					}else if(data == 2){
						alert("Please set disaster alert template first!");
					}else{
						alert("Error!");
					}
				}
			});
		}else{
			alert("Error!");
		}
 	}

 //  $("#alertAdd_form").submit( function(e){
	// 	e.preventDefault();
	// 	var formData = $(this).serialize();
	// 	var url = "ajax/alert_data_add.php";
	// 	$("#alert_add_md").modal("hide");
	// 	var level = $("#level").val();

	// 	if(level != 0){
	// 		$.ajax({
	// 			type: "POST",
	// 			url: url,
	// 			data: formData,
	// 			success: function(data){
	// 				if(data == 1){
	// 					alert("Alert was successfully added.");
	// 					$("input").val("");
	// 					$("textarea").html("");
	// 					$("select").val("-1");
	// 					getNotif();
	// 				}
	// 			}
	// 		});
	// 	}else{
	// 		alert("Please select danger level!.");
	// 	}
	// });

 //  function checkAllAlert(){
 //    var cb = $("#cb_alerts").is(":checked");

 //    if(cb){
 //      $("input[name=a_id]").prop("checked", true);
 //    }else{
 //      $("input[name=a_id]").prop("checked", false);
 //    }
 //  }

 //  function edit_alert(id){
 //  	$("#alert_edit_md").modal();
 //  	getAlertData(id);
 //  	$("#a_id").val(id);
 //  }

 //  function getAlertData(id){
 //  	$.ajax({
 //  		type: "POST",
 //  		url: "ajax/alert_data.php",
 //  		data: {aID: id},
 //  		success: function(data){
 //  			var o = JSON.parse(data);
 //  			$("#a_id").val(o.disaster_id);
 //  			$("#edit_a_title").val(o.disaster_title);
 //  			$("#edit_a_details").html(o.disaster_details);
 //  			$("#edit_a_level").val(o.disaster_danger_level);
 //  		}
 //  	});
 //  }

 //  $("#alertEdit_form").submit( function(e){
	// 	e.preventDefault();
	// 	var formData = $(this).serialize();
	// 	var url = "ajax/alert_data_edit.php";
	// 	$("#alert_edit_md").modal("hide");
	// 	var level = $("#edit_a_level").val();

	// 	if(level != 0){
	// 		$.ajax({
	// 			type: "POST",
	// 			url: url,
	// 			data: formData,
	// 			success: function(data){
	// 				if(data == 1){
	// 					alert("Disaster alert data was successfully updated.");
	// 					$("input").val("");
	// 					$("select").val("0");
	// 					getNotif();
	// 				}
	// 			}
	// 		});
	// 	}else{
	// 		alert("Please select danger level!.");
	// 	}
	// });

 //  function delete_alert(){
	// var con = confirm("Are you sure to delete selected?");

	// if(con)
	// {
	// 	var check = $("input[name=a_id]").is(":checked");
	//     var x = [];
	//     $("input[name=a_id]:checked").each( function(){
	//       x.push($(this).val());
	//     });

	//     if(check){
	//     	$.ajax({
	//     		type: "POST",
	//     		url: "ajax/alert_delete.php",
	//     		data: {aID: x},
	//     		success: function(data){
	//     			if(data == 1){
	//     				alert("Selected alert was deleted.");
	//     				getNotif();
	//     			}else{
	//     				alert("Error!");
	//     			}
	//     		}
	//     	});
	//     }else{
	//     	alert("Please select alert first!");
	//     }
	// }
 //  }

 //  //DISASTER ALERT END

  //DISASTER ALERT TEMP

  $("#alertAdd_form").submit( function(e){
		e.preventDefault();
		var formData = $(this).serialize();
		var url = "ajax/alert_temp_data_add.php";
		$("#alert_add_md").modal("hide");
		var level = $("#level").val();
		var a_title = $("#a_title").val();
		var evac_loc = $("#evac_loc").val();

		if(level != 0 && a_title != 0 && evac_loc != ""){
			$.ajax({
				type: "POST",
				url: url,
				data: formData,
				success: function(data){
					if(data == 1){
						alert("Alert Template was successfully added.");
						$("input").val("");
						$("textarea").val("");
						$("select").val("-1");
						getNotif_temp();
						deleteMarkers();
					}
				}
			});
		}else{
			alert("Please input required fields!.");
		}
	});

  function checkAllAlert(){
    var cb = $("#cb_alert_temp").is(":checked");

    if(cb){
      $("input[name=at_id]").prop("checked", true);
    }else{
      $("input[name=at_id]").prop("checked", false);
    }
  }

  function edit_alert(id){
  	$("#alert_edit_md").modal();
  	getAlertData(id);
  	$("#a_id").val(id);
  	initMap("map1");
  }

  function getAlertData(id){
  	deleteMarkers();
  	$.ajax({
  		type: "POST",
  		url: "ajax/alert_data.php",
  		data: {aID: id},
  		success: function(data){
  			var o = JSON.parse(data);
  			$("#a_id").val(o.temp_id);
  			$("#edit_a_title").val(o.disaster_title);
  			$("#edit_a_details").val(o.disaster_details);
  			$("#edit_a_level").val(o.disaster_danger_level);
  			var loc_lat = parseFloat(o.disaster_latitude);
  			var loc_long = parseFloat(o.disaster_longitude);
  			const loc = { lat: loc_lat, lng: loc_long };
  			const marker = new google.maps.Marker({
			    position: loc,
			    map: map,
			    label: "Evacuation Center"
			  });
			  markers.push(marker);
			$("#evac_loc1").val("("+loc_lat+","+loc_long+")");
  		}
  	});
  }

  $("#alertEdit_form").submit( function(e){
		e.preventDefault();
		var formData = $(this).serialize();
		var url = "ajax/alert_data_edit.php";
		$("#alert_edit_md").modal("hide");
		var level = $("#edit_a_level").val();
		var type = $("#edit_a_title").val();
		var evac_loc = $("#evac_loc1").val();

		if(level != 0 && a_title != 0 && evac_loc != ""){
			$.ajax({
				type: "POST",
				url: url,
				data: formData,
				success: function(data){
					if(data == 1){
						alert("Disaster alert template data was successfully updated.");
						$("input").val("");
						$("select").val("0");
						getNotif_temp();
					}
				}
			});
		}else{
			alert("Please select disaster type or danger level!.");
		}
	});

  function delete_alert(){
	var con = confirm("Are you sure to delete selected?");

	if(con)
	{
		var check = $("input[name=at_id]").is(":checked");
	    var x = [];
	    $("input[name=at_id]:checked").each( function(){
	      x.push($(this).val());
	    });

	    if(check){
	    	$.ajax({
	    		type: "POST",
	    		url: "ajax/alert_temp_delete.php",
	    		data: {aID: x},
	    		success: function(data){
	    			if(data == 1){
	    				alert("Selected alert template was deleted.");
	    				getNotif_temp();
	    			}else{
	    				alert("Error!");
	    			}
	    		}
	    	});
	    }else{
	    	alert("Please select alert template first!");
	    }
	}
  }

  function set_temp(id,dType,status){
  	$.ajax({
  		type: "POST",
  		url: "ajax/set_template_data.php",
  		data: {aID: id, dType: dType, status: status},
  		success: function(data){
  			if(data == 1){
				alert("Selected alert template status was updated.");
				getNotif_temp();
			}else{
				alert("Error!");
			}
  		}
  	});
  }

  //DISASTER ALERT TEMP END

  //SAFETY TIPS

  	$("#addTips_form").submit( function(e){
		e.preventDefault();
		var formData = new FormData(this);
		var url = "ajax/add_tips.php";
		$("#add_tips_md").modal("hide");
		var aud = $("#t_audience").val();

		if(aud != -1){
			$.ajax({
				type: "POST",
				url: url,
				processData: false,
				contentType: false,
				data: formData,
				success: function(data){
					if(data == 1){
						alert("Safety tips was successfully added.");
						$("input").val("");
						$("textarea").html("");
						$("select").val("-1");
						$("#e_prev_header").prop("src","https://dummyimage.com/450x200/9fa0a6/fff");
						getTips();
					}
				}
			});
		}else{
			alert("Please select an audience.");
		}
	});

	function delete_tips(){
	var con = confirm("Are you sure to delete selected?");

	if(con)
	{
		var check = $("input[name=t_id]").is(":checked");
	    var x = [];
	    $("input[name=t_id]:checked").each( function(){
	      x.push($(this).val());
	    });

	    if(check){
	    	$.ajax({
	    		type: "POST",
	    		url: "ajax/delete_tips.php",
	    		data: {uID: x},
	    		success: function(data){
	    			if(data == 1){
	    				alert("Selected safety tips was deleted.");
	    				getTips();
	    			}else{
	    				alert("Error:"+data);
	    			}
	    		}
	    	});
	    }else{
	    	alert("Please select safety tips first!");
	    }
	}
  }

   function edit_tips(id){
  	$("#edit_tips_md").modal();
  	getTipsData(id);
  	$("#t_id").val(id);
  }

  function getTipsData(id){
  	$.ajax({
  		type: "POST",
  		url: "ajax/tips_data.php",
  		data: {uID: id},
  		success: function(data){
  			var o = JSON.parse(data);
  			$("#t_id").val(o.safety_tips_id);
  			$("#t_title").val(o.title);
  			$("#t_content").html(o.content);
  			$("#te_audience").val(o.audience);
  			$("#t_prev_header").attr("src",o.image_path);
  		}
  	});
  }

  $("#editTips_form").submit( function(e){
		e.preventDefault();
		var formData = new FormData(this);
		var url = "ajax/edit_tips.php";
		$("#edit_tips_md").modal("hide");
		$.ajax({
			type: "POST",
			url: url,
			processData: false,
			contentType: false,
			data: formData,
			success: function(data){
				if(data == 1){
					alert("Safety tips data was successfully updated.");
					$("input").val("");
					$("select").val("0");
					getTips();
				}else{
					alert("Error: "+data);
				}
			}
		});
	});

  function getImage(){
  	var event = document.getElementById('theader');
  	var output = document.getElementById('tprev_header');
	document.getElementById('tprev_header').src = window.URL.createObjectURL(event.files[0]);
  }

  function getImage_edit(){
  	var event = document.getElementById('t_header');
  	var output = document.getElementById('t_prev_header');
	document.getElementById('t_prev_header').src = window.URL.createObjectURL(event.files[0]);
  }

  //SAFETY TIPS END
</script>