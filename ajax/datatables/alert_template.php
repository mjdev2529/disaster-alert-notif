<?php

	include '../../core/config.php';

	$sql = mysql_query("SELECT * FROM tbl_alert_template");

	$count = 1;
	$response['data'] = array();
	while($row = mysql_fetch_array($sql)){
		$list = array();

		if($row["disaster_title"] == 1){
			$d_type = "<span class='text-danger'>FIRE</span>";
		}else if($row["disaster_title"] == 2){
			$d_type = "<span class='text-warning'>EARTHQUAKE</span>";
		}else{
			$d_type = "<span class='text-info'>FLOOD/STORM</span>";
		}

		if($row["disaster_danger_level"] == 1){
			$d_level = "<span class='text-success'>Low</span>";
		}else if($row["disaster_danger_level"] == 2){
			$d_level = "<span class='text-warning'>Medium</span>";
		}else{
			$d_level = "<span class='text-danger'>High</span>";
		}

		$list['a_id'] = $row["temp_id"];
		$list['count'] = $count++;
		$list['title'] = $d_type;
		$list['date_added'] = date("Y-m-d", strtotime($row["date_added"]));
		$list['level'] = $d_level;
		$list['d_type'] = $row["disaster_title"];
		$list['set_temp'] = $row["set_template"];

		array_push($response['data'],$list);
	}

	echo json_encode($response);

?>