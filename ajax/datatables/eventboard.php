<?php

	include '../../core/config.php';

	$sql = mysql_query("SELECT * FROM tbl_eventboard");

	$count = 1;
	$response['data'] = array();
	while($row = mysql_fetch_array($sql)){
		$list = array();

		if($row["audience"] == 1){
			$ustat = "Teacher";
		}else if($row["audience"] == 2){
			$ustat = "Student";
		}else{
			$ustat = "All";
		}

		$list['event_id'] = $row["event_id"];
		$list['count'] = $count++;
		$list['title'] = $row['title'];
		$list['date_added'] = date("Y-m-d", strtotime($row["date_added"]));
		$list['audience'] = $ustat;

		array_push($response['data'],$list);
	}

	echo json_encode($response);

?>