<?php

	include '../../core/config.php';

	$sql = mysql_query("SELECT * FROM tbl_disaster_report");

	$count = 1;
	$response['data'] = array();
	while($row = mysql_fetch_array($sql)){
		$list = array();

		if($row["disaster_danger_level"] == 1){
			$d_level = "<span class='text-success'>Low</span>";
		}else if($row["disaster_danger_level"] == 2){
			$d_level = "<span class='text-warning'>Medium</span>";
		}else{
			$d_level = "<span class='text-danger'>High</span>";
		}

		$list['a_id'] = $row["disaster_id"];
		$list['count'] = $count++;
		$list['title'] = $row['disaster_title'];
		$list['date_added'] = date("Y-m-d", strtotime($row["date_added"]));
		$list['level'] = $d_level;

		array_push($response['data'],$list);
	}

	echo json_encode($response);

?>