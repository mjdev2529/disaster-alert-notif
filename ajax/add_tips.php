<?php
	include '../core/config.php';

	$title = $_POST["title"];
	$content = $_POST["content"];
	$audience = $_POST["audience"];
	$date_added = date("Y-m-d");

	$target_dir = "../assets/images/";
	$slug = "assets/images/" . basename($_FILES["header"]["name"]);
	$target_file = $target_dir . basename($_FILES["header"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

	// Check if image file is a actual image or fake image
	if(isset($_FILES["header"])) {
	  $check = getimagesize($_FILES["header"]["tmp_name"]);
	  if($check !== false) {
	    $uploadOk = 1;
	  } else {
	    echo "File is not an image.";
	    $uploadOk = 0;
	  }
	}

	// Check file size
	if ($_FILES["header"]["size"] > 500000) {
	  echo "Sorry, your file is too large.";
	  $uploadOk = 0;
	}

	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	  $uploadOk = 0;
	}

	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	  echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	  if (move_uploaded_file($_FILES["header"]["tmp_name"], $target_file)) {
	    $proceed = 1;
	  } else {
	    $proceed = 0;
	  }
	}

	if($proceed == 1){
		$add = mysql_query("INSERT INTO tbl_safety_tips SET title = '$title', content = '$content', audience = '$audience', date_added = '$date_added', image_path = '$slug'") or die(mysql_error());
		if($add){
			echo 1;
		}else{
			echo 0;
		}
	}else{
		echo 0;
	}

?>