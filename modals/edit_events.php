<!-- Modal -->
<div class="modal fade" id="edit_event_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i data-feather="edit-3"></i> Edit Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="editEvent_form" class="form-row" method="post" enctype="multipart/form-data">
            <div class="col-md-12 mb-2" style="max-width: 450px;max-height: 200px;">
              <img id="e_prev_header" src="https://dummyimage.com/450x200/9fa0a6/fff" height="200px" width="450px" style="margin-left: 2%;">
            </div>
            <div class="col-md-8 offset-md-2">
              <div class="form-group">
                <label for="inputEmail4">Event Header</label>
                <input type="file" class="form-control" id="e_header" name="header" oninput="getImage_edit_event()" accept="image/*"  >
              </div>
              <div class="form-group">
                <label for="inputEmail4">Title</label>
                <input type="hidden" id="e_id" name="e_id">
                <input type="text" class="form-control" id="e_title" name="title" placeholder="Title" required="">
              </div>
              <div class="form-group">
                <label for="inputPassword4">Content</label>
                <textarea class="form-control" cols="2" placeholder="Type Here..." id="e_content" name="content" required=""></textarea>
              </div>
              <div class="form-group">
                <label for="inputState">Audience</label>
                <select class="form-control" name="audience" id="e_audience">
                  <option selected value="-1">Please choose audience:</option>
                  <option value="0">All</option>
                  <option value="1">Teacher</option>
                  <option value="2">Student</option>
                </select>
              </div>
            </div>
            <button type="submit" class="btn btn-success offset-md-4">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>