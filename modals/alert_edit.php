<!-- Modal -->
<div class="modal fade" id="alert_edit_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i data-feather="edit-3"></i> Edit Disaster Alert</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="alertEdit_form" class="form-row" method="post">
            <div class="col-md-6">
              <div class="form-group">
                <label for="inputEmail4">Set evacuation site</label>
                <div class="btn-group mb-1 float-right">
                  <button type="button" class="btn btn-sm" onclick="clearMarkers()"><i class="fa fa-trash"></i></button>
                </div>
                <div class="col-md-12" id="map1"></div>
                <input type="hidden" name="evac_loc" id="evac_loc1">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="inputEmail4">Title</label>
                <input type="hidden" id="a_id" name="a_id">
                <select class="form-control" name="title" id="edit_a_title">
                  <option selected value="0">Please choose:</option>
                  <option value="1">Fire</option>
                  <option value="2">Earthquake</option>
                  <option value="3">Flood/Storm</option>
                  <!-- <option value="4">Tsunami</option> -->
                </select>
              </div>
              <div class="form-group">
                <label for="inputPassword4">Details</label>
                <textarea class="form-control" cols="2" placeholder="Type Here..." id="edit_a_details" name="details" required=""></textarea>
              </div>
              <!-- <div class="form-group">
                <label for="inputState">Danger Level</label>
                <select class="form-control" name="level" id="edit_a_level">
                  <option selected value="0">Please choose level:</option>
                  <option value="1">Low</option>
                  <option value="2">Medium</option>
                  <option value="3">High</option>
                </select>
              </div> -->
            </div>
            <button type="submit" class="btn btn-success offset-md-10">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>