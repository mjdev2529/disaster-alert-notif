<!-- Modal -->
<div class="modal fade" id="edit_user_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i data-feather="edit-3"></i> Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="editUser_form" class="form-row">
            <div class="col-md-8 offset-md-2">
              <div class="form-group">
                <label for="inputEmail4">First Name</label>
                <input type="text" class="form-control" id="e_ufname" name="fname" placeholder="First Name">
                <input type="hidden" id="u_id" name="u_id">
              </div>
              <div class="form-group">
                <label for="inputPassword4">Middle Initial</label>
                <input type="text" class="form-control" id="e_umname" name="mname" placeholder="Middle Initial" maxlength="1">
              </div>
              <div class="form-group">
                <label for="inputPassword4">Last Name</label>
                <input type="text" class="form-control" id="e_ulname" name="lname" placeholder="Last Name">
              </div>
              <div class="form-group">
                <label for="inputAddress">Username</label>
                <input type="text" class="form-control" id="e_uname" name="uname" placeholder="Username">
              </div>
              <div class="form-group">
                <label for="inputAddress2">Password</label>
                <input type="password" class="form-control" id="e_upass" name="pass" placeholder="Password">
              </div>
              <div class="form-group">
                <label for="inputState">Status</label>
                <select id="e_role" class="form-control" name="role">
                  <option selected value="0">Please choose status:</option>
                  <option value="1">Teacher</option>
                  <option value="2">Student</option>
                </select>
              </div>
            </div>
            <button type="submit" class="btn btn-success offset-md-4">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>